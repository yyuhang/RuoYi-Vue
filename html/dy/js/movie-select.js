
var cinemaData = JSON.parse(decodeURI(getHashParameter("cinemaData")))
var filmId = getHashParameter("id")
$(".crumbs-nav .i2").html(cinemaData.name)
$(".cinema-info .title").html(cinemaData.name)
$(".cinema-info .location").html(cinemaData.address)
$(".nav-header").html(cinemaData.name + ' ' + cinemaData.address)
$(".cinema-info .location-icon").click(function () {
    window.location.href = 'https://map.baidu.com/mobile/webapp/search/search/qt=s&wd=' + cinemaData.name + cinemaData.address + '&vt=map'
})




var paiqList = []
var filmPaiqList = []
var swiper1 = null
var paiqData1 = {}


function setPaiqInfo(info) {
    $(".movie-info .title").html(info.name)
    paiqData1 = info
    $(".movie-info .movie-desc span").eq(0).html(info.country + ' | ')
    $(".movie-info .movie-desc span").eq(1).html(info.type + ' | ')
    $(".movie-info .movie-desc span").eq(2).html(info.cast)
}

function filmHtml(data) {
    var dom = `<div class="swiper-slide" data-id="${data.code}"> 
        <div class="post">
            <img src="${data.poster}">
        </div>
    </div>`

    return dom
}
// 获得影院下的电影
getFilmList(filmId, function (res) {
    if (typeof (res) == 'string') {
        res = JSON.parse(res)
    }
    paiqList = res.result
    $("#swiper-wrapper").html(' ')
    //console.log(res.result) 
    for (var i = 0; i < res.result.length; i++) {
        $("#swiper-wrapper").append(filmHtml(res.result[i]))
    }
    getData(res.result[0])
    new Swiper('.cinema-nav', {
        // 高度自适应
        slidesPerView: 4,
        centeredSlides: true,
        on: {
            slideChangeTransitionEnd: function (e) {
                //console.log(e)
                $('.cinema-nav').find('.swiper-slide').eq(e.activeIndex)
                getData(res.result[e.activeIndex])
            },
        },
        slideToClickedSlide: true
    });
})

function getData (data) {
    getFilmPaiq(data.code) 
    setPaiqInfo(data)
}

function swiperHtml(data) {
    var dom = `<div class="swiper-slide" data-time="${timestampToTimeNoY(data.show_time)}">
            <div class="nav-item" data-bid="dp_wx_cinema_show_date" data-id="today-no-movie">
                <span class="date-title">${timestampToTimeNoY(data.show_time)}</span>
            </div>
        </div>`
    return dom
}



// 获得影院下的电影排期时间
function getFilmPaiq(id) {
    getFilmPaiqiList(id,filmId, function (res) {
        if (typeof (res) == 'string') {
            res = JSON.parse(res)
        }
        filmPaiqList = res.result.data
        $("#swiper-wrapper-date").html(" ")
        // 循环出电影排期时间
        for (var i = 0; i < res.result.data.length; i++) {
            if ($(`#swiper-wrapper-date .swiper-slide[data-time='${timestampToTimeNoY(res.result.data[i].show_time)}']`).length != 0) {
            } else {
                $("#swiper-wrapper-date").append(swiperHtml(res.result.data[i]))
            }
        }
        if (swiper1) {
            swiper1.destroy(true);
        }
        $(".swiper-container1 .swiper-slide").eq(0).find(".nav-item").addClass("active")
        setPaiqHtml(timestampToTimeNoY(res.result.data[0].show_time))
        swiper1 = new Swiper('.swiper-container1', {
            // 高度自适应
            slidesPerView: 3,
            autoplay: false,
            on: {
                //swiper从当前slide开始过渡到另一个slide时执行
                tap: function () {
                    swiper1.slideTo(this.clickedIndex - 1, 1000, false);
                    $(".swiper-container1 .swiper-slide").find(".nav-item").removeClass("active")
                    $(".swiper-container1 .swiper-slide").eq(this.clickedIndex).find(".nav-item").addClass("active")
                    setPaiqHtml($(".swiper-container1 .swiper-slide").eq(this.clickedIndex).attr('data-time'))
                }
            }
        });
    })
}

function filmPaiqHtml(data) {
    var dom = `<div class="item-outer mb-line-b" data-bid="dp_wx_cinema_show_item">

        <a data-link='seats.html#data=${encodeURI((JSON.stringify(data)))}' class="item box-flex llad">
            <div class="time-block">
                <div class="begin">${getSFTime(data.show_time)}</div>
                <div class="end">${getSFTime(data.close_time)}<span class="tui">散场</span></div>
            </div>
            <div class="info-block">
                <div class="lan">${data.show_version}</div>
                <div class="hall">${data.hall_name}</div>
            </div>
            <div class="price">
                <div class="sellPr"><span class="d">¥</span><span><span class="stonefont">${data.price / 100}</span></span></div>

            </div>
            <div class="button-block">
                <div class="button" data-bid="dp_wx_cinema_show_btn">购票</div>
            </div>
        </a>
    </div>`
    return dom
}

$("body").on('click', '.llad', function() {
    window.location.href = $(this).attr('data-link') + '&name='+encodeURI(paiqData1.name)+ '&img='+encodeURI(paiqData1.poster)+ '&cinemaname='+encodeURI(cinemaData.name)
})

function setPaiqHtml(date) {
    let length = 0
    $("#paiqList").html(' ')
    $("#nodata").hide()
    for (var i = 0; i < filmPaiqList.length; i++) {
        if (timestampToTimeNoY(filmPaiqList[i].show_time) == date) {
            length++
            $("#paiqList").append(filmPaiqHtml(filmPaiqList[i]))
        }
    }
    if (length == 0) {
        $("#nodata").show()
    }
}





