var cinemaData = JSON.parse(decodeURI(getHashParameter("data")))
var cinemaName = decodeURI(getHashParameter("name"))
var pqData = {}
var leftWidth = 0

$(".nav-header").html(cinemaName)
$(".movie-info .title").html(cinemaName)
$(".movie-info .info span").eq(0).html(timestampToTime123(cinemaData.show_time))
$(".movie-info .info span").eq(1).html(cinemaData.show_version)
$(".hall-name").html(cinemaData.hall_name)
$(".model-tes-des").html(`您选的是 <span>${timestampToTime(cinemaData.show_time)}</span> 的场次，请您仔细核对，祝您观影愉快！`) 

// 隐藏座位
function hideSeating () {
    var dom = `<div class="wrap">
            <div class="seat"></div>
        </div>`
   return dom
}
// 可选座位， 普通座位
function optionalSeating1 (data) {
    var dom = `<div class="wrap" data-status="1" data-id="${data.id}" data-info='${JSON.stringify(data)}' data-row="${data.row}" data-column="${data.column}">
            <div class="seat"></div>
        </div>`
   return dom
}
// 可选座位， 情侣座位
function optionalSeating2 (data) {
    var dom = `<div class="wrap" data-status="1" data-filg="${data.flag}" data-id="${data.id}" data-info='${JSON.stringify(data)}' data-row="${data.row}" data-column="${data.column}">
        <div class="seat" style="background-image:url(images/fszw.png)"></div>
    </div>`
    return dom
}

// 已售座位
function soldSeating (data) {
    var dom = `<div class="wrap" data-row="${data.row}" data-info='${JSON.stringify(data)}' data-column="${data.column}">
        <div class="seat" style="background-image:url(https://p0.meituan.net/moviemachine/2af4c987b1758563e22258c077139cdf842.png)"></div>
    </div>`
    return dom
}

// 不可选座位
function noOptionalSeating (data) {
    var dom = `<div class="wrap" data-row="${data.row}" data-info='${JSON.stringify(data)}' data-column="${data.column}">
        <div class="seat" style="background-image:url(https://p0.meituan.net/moviemachine/4abd89a0c0a864694b27c54d229fa7f41099.png)"></div>
    </div>`
    return dom
}
Qmsg.loading('正在加载座位图, 请稍等')
initSeating()

$(".model-tes-btn").click(function(){
    $(".model-tes").hide()
})

// 渲染座位
function initSeating () {
    
    getSeatingList(cinemaData.id, function (res) {
        Qmsg.closeAll()
        $(".model-tes").show()
        if (typeof (res) == 'string') {
            res = JSON.parse(res)
        }
        pqData = res.result[0]
        var data = res.result[0]
        var seats = data.seats
        // 公告
        $('.reminder-list .reminder-item').eq(0).find('div').html(data.notice)
        // 左边行
       

        // 设置宽
        $("#seats-wrap").css('width', (46 * data.maxcolumn) + 40)
        
        $("#seats-wrap-box").css({
            width: (46 * data.maxcolumn) + 40
        })
        leftWidth = ((46 * data.maxcolumn) - 200) / 2
        $("#hall-name-wrapper").css({
            transform: `translate3d(${((46 * data.maxcolumn) - 200) / 2 }px, 0px, 0px) scale(1, 1) rotate3d(0, 0, 0, 0deg)`
        })
        
        let columnAll = data.maxcolumn
        let columnIdx = 0
        let rowAll = 1
        let bs = 0
        for(var i = 0; i < (data.maxcolumn * data.maxrow); i++) {
            let dom = ''
            columnIdx ++
            if (seats[i - bs] && columnIdx ==  seats[i - bs].column && seats[i - bs].row == rowAll) {
                // 可选座位
                if (seats[i- bs].status == 1) {
                    
                    if (seats[i- bs].flag >= 1) {
                        dom = optionalSeating2(seats[i- bs])
                    } else {
                        dom = optionalSeating1(seats[i- bs])
                    }
                } 
                // 已售座位
                else if (seats[i- bs].status == 0) {
                    dom = soldSeating(seats[i- bs])
                } 
                // 不可选座位
                else if (seats[i- bs].status == -1) {
                    dom = noOptionalSeating(seats[i- bs])
                } else {
                   
                }
            } else {
                bs++
                dom = hideSeating()
            }
            if (columnIdx == columnAll) {
                $("#row").append(`<div>${seats[i- bs].row_name}</div>`)
                columnIdx = 0
                rowAll++
            }
            $("#seats-wrap").append(dom)
        }

        
        let soc = 0.5
        if ($("#seats-wrap-box").width() <= ($("body").width() + 200)) {
            soc = 1
        }
        
        myScroll = new iScroll("seat-block-wrap",{zoom:true,zoomMin:soc,bounce:false,momentum:true,hScrollbar:false,vScrollbar:false,fixedScrollbar:true});
        let leftX = (($("body").width() - ($("#seats-wrap-box").width())) / 2)
        let widD = 0
        if (soc != 1) {
            leftX = (($("body").width() - ($("#seats-wrap-box").width() / 2)) / 2)
            widD = Math.abs($("body").width() - ($("#seats-wrap-box").width() / 2))

            if (($("#seats-wrap-box").width() / 2) <= $("body").width()) {
                widD = -($("body").width() - ($("#seats-wrap-box").width() / 2))
            }
        }
        myScroll.zoom(leftX, 0, soc, 200);
        $("#row").css({
            transform: `translate3d(${widD}px, 0px, 0px) scale(1, 1) rotate3d(0, 0, 0, 0deg)`
        })
        document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);

    })
}





var dx = 0
var dy = 0

function isCrollCallback (x, y, scale) {
    $("#hall-name-wrapper").css({
        transform: `translate3d(${leftWidth}px, ${Math.abs(y)}px, 0px) scale(1, 1) rotate3d(0, 0, 0, 0deg)`
    })
    $("#row").css({
        transform: `translate3d(${Math.abs(x) * (scale == '0.5' ? 2 : 1)}px, 0px, 0px) scale(1, 1) rotate3d(0, 0, 0, 0deg)`
    })
    if (x ==0) {
        $("#row").css({
            transform: `translate3d(${0}px, 0px, 0px) scale(1, 1) rotate3d(0, 0, 0, 0deg)`
        })
    }
}



let clickdyc = true
let clickdyc1 = true
let selectData = []
$('body').on("click", '.wrap', function() {
    
    if (clickdyc1) {
        myScroll.scrollTo((-$(this).position().left * 2) + 100, (-$(this).position().top * 2) + 100, 0, 0)
        myScroll.zoom((-$(this).position().left * 2) + 100, (-$(this).position().top * 2) + 100, 1, 0);
        clickdyc1 = false
        return
    }

    if ($(this).attr("data-status") == '1') {
        if ($(this).hasClass('active')) {
            delectSelect($(this).attr('data-id'))
            return
        }
        if (selectData.length == pqData.maxcanbuy) {
            Qmsg.warning('最多只能选择' +pqData.maxcanbuy+'个座位')
            return
        }
        myScroll.zoom(-$(this).position().left - (-$(this).offset().left), -$(this).position().top - -($(this).offset().top - 132), 1, 0);
        
        $(this).addClass('active')
        
        $(".price-block").show()
        
        $(".selected-block").append(addText(JSON.parse($(this).attr("data-info"))))
        selectData.push(JSON.parse($(this).attr("data-info")))
        setShowPrice()
    }
})

function setShowPrice () {
    if (selectData.length != 0) {
        $(".buy-block").attr("data-show", "submit")
        $(".buy-block .submit").html('¥' + (selectData.length * (cinemaData.price / 100)).toFixed(2) + '确认选座')
    }
    else {
        $(".buy-block").attr("data-show", "")
        $(".buy-block .submit").html('请先选座')
    }
}

$('.submit-block .submit').click(function() {
    // 过滤是否符合选择规则
    if (filterYes()) {
        return
    }
    if (selectData.length == 0) {
        Qmsg.warning('请确认选座')
        return
    }
    // window.location.href = 'createOrder.html'
    $(".model-form1").show()
})
$(".clobs").click(function(){
    $(".model-form1").hide()
})
function IsCard(str) {
    var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return reg.test(str);
}
function validatePhoneNumber(str) {
    const reg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
    return reg.test(str)
   }
$("#confirm-submit-btn").click(function() {
    // 过滤是否符合选择规则
    if ($(".name-input").val().length == 0) {
        Qmsg.warning('请输入真实姓名')
        return
    }
    if ($(".card-input").val().length == 0 && IsCard($(".card-input").val()) == false) {
        Qmsg.warning('请输入合法的证件号')
        return
    }
    if ($(".phone-input").val().length == 0 && validatePhoneNumber($(".phone-input").val()) == false) {
        Qmsg.warning('请输入合法的手机号')
        return
    }

    var names = []
    var ids = []
    for(var i =0; i < selectData.length; i++) {
        names.push(selectData[i].name)
        ids.push(selectData[i].id)
    }
    
    Qmsg.loading('正在锁座， 请稍等')
    // var res = {
    //     Order_Id: 1,
    //     Count: 3,
    //     Placed_time: 1522287039,
    //     Expire_time: 1522287039
    // }
    // window.location.href = `createOrder.html${window.location.hash}&orderData=${encodeURI(JSON.stringify(res))}&phone=${encodeURI($(".phone-input").val())}&names=${encodeURI(JSON.stringify(names))}`
    // 锁座 请求  
    seatLook({
        seat_names: names.toString(),
        paiqi_id: cinemaData.id,
        seat_ids: ids.toString(),
        phone_num: $(".phone-input").val(),
        zhengjianming: $(".name-input").val(),
        zhengjiantype: $(".card-type").val(),
        zhengjianid: $(".card-input").val(),
		seat_areas:"1",
		account_id:"1"
    }, function (res) {

        // window.location.href = `createOrder.html${window.location.hash}&orderData=${encodeURI(JSON.stringify(res.result))}&phone=${encodeURI($(".phone-input").val())}&names=${encodeURI(JSON.stringify(names))}`
        if (res.code == 200) {
            window.location.href = `createOrder.html${window.location.hash}&orderData=${encodeURI(JSON.stringify(res.result))}&phone=${encodeURI($(".phone-input").val())}&names=${encodeURI(JSON.stringify(names))}`
        } else {
            window.location.href = `error.html`
        }
    })
   
})

function filterYes() {
    var err = false
    $(".wrap.active").each(function() {
        if (err) {
            return
        }
        
        if ($(this).attr('data-filg')) {
            if ($(this).attr('data-filg') == 1 && !$(this).next().hasClass('active')) {
               
                err = true
            } 
            if ($(this).attr('data-filg') == 2 && !$(this).prev().hasClass('active')){
                console.log(111)
                err = true
            }
        } else {
            if ($(this).prev().attr('data-status') == 1 && !$(this).prev().hasClass('active')) {
            
                err = true
            }
    
            if ($(this).prev().attr('data-status')) {
                if ($(this).prev().prev().attr('data-status') == 1 && !$(this).prev().prev().hasClass('active')) {
                    err = false
                }
                if (!$(this).prev().prev().attr('data-status')) {
                    err = true
                    if (!$(this).next().attr('data-status')) {
                        err = false
                    }
                }
            }
            
            if (!err) {
                if ($(this).next().attr('data-status') == 1 && !$(this).next().hasClass('active')) {
                    err = true
                }
                if ($(this).next().attr('data-status')) {
                    if ($(this).next().next().attr('data-status') == 1 && !$(this).next().next().hasClass('active')) {
                        err = false
                    }
                    if (!$(this).next().next().attr('data-status')) {
                        err = true
                        if (!$(this).prev().attr('data-status')) {
                            err = false
                        }
                    }
                }
               
            }
        }


        if (err) {
            Qmsg.warning('旁边座位不要留空，请选择连在一起的座位')
        }
        
    })
    return err
}


var show = false
$(".reminder-num").click(function() {
    if (!show) {
        $("i").addClass('fold-up').removeClass('fold-down')
        $(".reminder-list .reminder-item").removeClass('height20').removeClass('hide')
    } else {
        $("i").addClass('fold-down').removeClass('fold-up')
        $(".reminder-list .reminder-item").addClass('height20')
        $(".reminder-list .reminder-item").eq(1).addClass('hide')
    }
    show = !show
    
})

function addText(info) {
    var text = $('<div class="selected-seat-item" data-id="'+info.id+'"> '+
                        '<p class="selected-seat-info"></p>'+

                        '<p class="price-info">34.00</p>'+

                        '<span class="close"  data-id="'+info.id+'"></span>'+
    '</div>')
    text.find('.selected-seat-info').html(info.name)
    text.find('.price-info').html(cinemaData.price / 100)
    return text
}


$("body").on('click', '.selected-seat-item .close', function () {
    var id = $(this).attr('data-id')
    $(this).parent(".selected-seat-item").remove()
    delectSelect(id)
})




function delectSelect (id) {
    let newSelectData = []
    for(var i = 0; i < selectData.length; i++) {
        if (selectData[i].id != id) {
            newSelectData.push(selectData[i])
        }
    }
    selectData = newSelectData
    $(`.wrap[data-id=${id}]`).removeClass('active')
    $(`.selected-seat-item[data-id=${id}]`).remove()
    setShowPrice()
}








 // var notice = data.notice.split("、")
        // for(var i = 0; i < notice.length; i++) {
        //     if (i == 0) {
        //         $('.reminder-list .reminder-item').eq(0).find('div').html(notice[i])
        //     }
        // }


        
// 公告
// function noticeHtml (data) {
//     var dom = `<li class="reminder-item  hide">
//         <img src="https://p1.meituan.net/movie/77717de09967c29cd5b3d1f76309ac841254.png">
//         <div>${data}</div>
//     </li>`
// }
