var cinemaData = JSON.parse(decodeURI(getHashParameter("cinemaData")))
console.log(cinemaData)
$(".crumbs-nav span").html(cinemaData.name)
$(".zwmz").html(cinemaData.name)
$(".ywmz").html(cinemaData.Show_name_en)
$(".movie-cat").html(cinemaData.type)
$(".yylb").html('演员: ' + cinemaData.cast)
$(".dylb").html('导演: ' + cinemaData.director)
$(".nav-header").html(cinemaData.name)

$(".datgs span").html(timestampToTime123(cinemaData.publishDate) + '上映')
$(".movie-show-country span").html(cinemaData.country + '  ' + cinemaData.language)
$(".poster").attr('src', cinemaData.poster)
$(".score").html(cinemaData.remark)


function stillImgHtml(data) {
    var dom = `<li class="left-margin">
                <a>
                    <img class="img" alt="avatar" src="${data}" >
                </a>
            </li>`
    return dom
}

var stills = cinemaData.stills.split(",")

for(var i = 0; i < stills.length; i++) {
    $("#photos-list").append(stillImgHtml(stills[i]))
}

$(".cinema-info .location-icon").click(function () {
    window.location.href = 'https://map.baidu.com/mobile/webapp/search/search/qt=s&wd=' + cinemaData.name + cinemaData.address + '&vt=map'
})