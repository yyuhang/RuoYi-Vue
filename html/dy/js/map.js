
var geoc = new BMap.Geocoder();
if (!localStorage.getItem("cityName")) {
    getMap()
}

function getMap() {
	var map = new BMap.Map("allmap");
	var point = new BMap.Point(116.501573, 39.900877);
	map.centerAndZoom(point, 16)
	console.log(22)
	// 此处二种方案可选其一，自测方案2更准确，1和2的方案，大致位置来讲都是准的
	// 定位对象方案1 : 百度获取经纬度

	var geolocation = new BMap.Geolocation();
	geolocation.enableSDKLocation(); 
	geolocation.getCurrentPosition(function (r) {
		if (this.getStatus() == BMAP_STATUS_SUCCESS) {
			var mk = new BMap.Marker(r.point);
			map.addOverlay(mk);
			map.panTo(r.point);

			localStorage.setItem("cityName", r.address.city)
			
			$(".city-name").html(r.address.city)
			if (typeof (api_film_city_list) == 'string') {
				api_film_city_list = JSON.parse(api_film_city_list)
			}
			for(var i = 0; i < api_film_city_list.result.length; i++) {
				if (r.address.city.includes(api_film_city_list.result[i].city_name)) {
					localStorage.setItem("cityId", api_film_city_list.result[i].city_code)
					getData(type)
				}
			}
		} else {
			
			console.log('无法定位到您的当前位置，导航失败，请手动输入您的当前位置！' + this.getStatus());
		}
	}, function(e) {
		console.log(e)
		Qmsg.warning(e.nf)
	}, { enableHighAccuracy: true });
}