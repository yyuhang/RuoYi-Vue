// 默认地址
var cityName = '北京'
var cityId = '110100'
// 先获取到当前的位置, 判断之前是否存储城市数据
if (localStorage.getItem("cityName")) {
    cityName = localStorage.getItem("cityName")
    cityId = localStorage.getItem("cityId")
}
// 展示选择地址名称
$(".city-name").html(cityName)

var type = getHashParameter("showContent")

if (type) {
    $('.switch-hot a[data-tab="' + type + '"]').addClass('active').siblings().removeClass('active')
    $(type).addClass('active').siblings().removeClass('active')
    $('.switch-hot').attr('data-active', type)
} else {
    type = '.n-hot'
}


// 请求数据
getData(type)

function getData(type) {
    
    // 请求热映影片
    if (type == '.n-hot') {
        getHotListFn()
    }
    // 获得即将上映
    if (type == '.f-hot') {
        getRightnowListFn()
    }

    // 查询影院列表
    if (type == '.cinema') {
        getCinemaListFn()
    }
}


function hotHtml (data) {
    var dom = `<div class="item" data-id="1228788" data-bid="dp_wx_home_movie_list">
            <div class="main-block">
                <div class="avatar" sort-flag="">
                    <div class="default-img-bg">
                        <img src="${data.poster}"
                            onerror="this.style.visibility='hidden'">
                    </div>
                </div>
                <div class="mb-outline-b content-wrapper">
                    <div class="column content">
                        <div class="box-flex movie-title">
                            <div class="title line-ellipsis ">${data.name}</div>
                        </div>

                        <div class="detail column">
                            <div class="score line-ellipsis">
                                <span class="score-suffix">观众评 </span>
                                <span class="grade">${data.remark}</span>
                            </div>
                            <div class="actor line-ellipsis">主演: ${data.cast}</div>
                            <div class="show-info line-ellipsis">类型: ${data.type}</div>
                        </div>
                    </div>
                    <a href="movie.html" class="button-block" data-id="1228788">
                        <div class="btn normal"><span class="fix"
                                data-bid="dp_wx_home_movie_btn">购票</span></div>
                    </a>
                </div>
            </div>
        </div>`
    return dom
}

// 请求热映影片
function getHotListFn() {
    getHotList(cityId, function (res) {
        if (typeof (res) == 'string') {
            res = JSON.parse(res)
        }
        $("#hotHtml").html(' ')
        for(var i = 0; i < res.result.length; i++) {
            $("#hotHtml").append(hotHtml(res.result[i]))
        }
    })
}


function rightnowHtml (data) {
    console.log(data)
    var dom = `<div class="item" data-id="1282401">
                <div class="main-block">
                    <a data-href='movie-introduction.html#cinemaData=${encodeURI((JSON.stringify(data)))}' class="avatar a-link">
                        <div class="default-img-bg">
                            <img src="${data.poster}">
                        </div>
                    </a>
                    <div class="mb-outline-b content-wrapper">
                        <a  data-href='movie-introduction.html#cinemaData=${encodeURI((JSON.stringify(data)))}' class="column a-link content">
                            <div class="box-flex movie-title">
                                <div class="title line-ellipsis ">${data.name}</div>
                            </div>
                            <div class="detail column">
                                <div class="actor line-ellipsis">类型: ${data.type}</div>
                                <div class="actor line-ellipsis">主演: ${data.cast}</div>
                                <div class="actor line-ellipsis">地区: ${data.country} - ${data.language}</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>`
    return dom
}
// 获得即将上映
function getRightnowListFn() {
    getRightnowList(cityId, function (res) {
        if (typeof (res) == 'string') {
            res = JSON.parse(res)
        }
        $("#rightnowHtml").html(' ')
        for(var i = 0; i < res.result.length; i++) {
            var domBox = ''
            if ($(`.ryp[data-date="${res.result[i].publishDate}"]`).length) {
                domBox = $(`.ryp[data-date="${res.result[i].publishDate}"]`)
            } else {
                domBox = $(`<div class="ryp" data-date="${(res.result[i].publishDate)}"><p class="group-date">${timestampToTime(res.result[i].publishDate)}</p></div>`)
            }
            $("#rightnowHtml").append(domBox.append(rightnowHtml(res.result[i])))
        }
    })
}



function cinemaHtml(data) {
    console.log(JSON.stringify(data))
    var dom = `<a data-href='movie-select.html#id=${data.code}&cinemaData=${encodeURI((JSON.stringify(data)))}' class="a-link">
        <div class="item mb-line-b" data-id="181" data-bid="dp_wx_home_cinema_list">
            <div class="title-block box-flex middle">
                <div class="title line-ellipsis">
                    <span>${data.name}</span>
                    <span class="price-block">
                        <span class="price">${data.Lowest_price}</span><span class="q">元起</span>
                    </span>
                </div>
                <div class="location-block box-flex">
                    <div class="flex line-ellipsis">地址：${data.address}</div>
                </div>
                <div class="location-block box-flex">
                    <div class="flex line-ellipsis">电话：${data.tel}</div>
                </div>
                <div class="discount-block">
                </div>
            </div>

        </div>
    </a>`
    return dom
}

// 查询影院列表
function getCinemaListFn() { 
    getCinemaList(cityId,cityName, function (res) {
        if (typeof (res) == 'string') {
            res = JSON.parse(res)
        }
        $("#cinemaHtml").html(' ')
        for(var i = 0; i < res.result.length; i++) {
            $("#cinemaHtml").append(cinemaHtml(res.result[i]))
        }
    })
}

$("body").on('click', '.a-link', function () {
    window.location.href = $(this).attr('data-href')
})







$(".switch-hot a").click(function () {
    $(this).addClass('active').siblings().removeClass('active')
    $($(this).attr('data-tab')).addClass('active').siblings().removeClass('active')
    $('.switch-hot').attr('data-active', $(this).attr('data-tab'))
    window.location.hash = 'showContent=' + $(this).attr('data-tab')
    getData($(this).attr('data-tab'))
})





$(".singleton").height($(window).height() - 95.5)
$(window).resize(function () {
    $(".singleton").height($(window).height() - 95.5)
});
$(".filter-nav-wrap .tab.mb-line-b .item").click(function () {
    $(this).addClass('chosenTitle').siblings().removeClass('chosenTitle')
    $(".close-tab").show()
    $(".blacker").show()
    $($(this).attr('data-tab')).show().siblings().hide()
})

$(".district-li").click(function () {
    $(this).addClass('chosen').siblings().removeClass('chosen')
})

$("#region-tab .item").click(function () {
    $(this).addClass('chosen').siblings().removeClass('chosen')
})

$(".brand-list").click(function () {
    $(this).addClass('chosen').siblings().removeClass('chosen')
    $(".filter-nav-wrap .tab.mb-line-b .item").removeClass('chosenTitle')
    $(".close-tab").hide()
    $(".blacker").hide()
    $(".tab-content.close-tab").hide()
})

$(".blacker").click(function () {
    $(".filter-nav-wrap .tab.mb-line-b .item").removeClass('chosenTitle')
    $(".close-tab").hide()
    $(".blacker").hide()
    $(".tab-content.close-tab").hide()
})







