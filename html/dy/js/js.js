$(".nav-button").click(function () {
	$(".nav-list").toggle()
})

const WEBURL = 'http://vip.dinglicard.com/dy-api'
// const WEBURL = 'http://localhost:8080/filmapi'  
const ACCOUNTID = ''
const SECRET = ''
var TOKEN = "";
// 是否测试环境
const TEST = false

var token_login = ''

function getToken() {  
	if (TEST) {
		return
	}
	
	TOKEN=""+$.session.get('token')+"";
	if(!TOKEN||TOKEN==""){
		alert("未登录");
		window.location.href="../shop/login-card.html";
		return;
	}
	
	// $.post(WEBURL + "/oauth/token", {
	// 	Account_id: ACCOUNTID,
	// 	secret: SECRET
	// }, function (res) {
	// 	TOKEN = res.result.Access_token
	// 	console.log(res)
	// });
	
	
	
}
// 自动获取token
getToken()

// 获得城市列表
function getCityList(callback) {
	if (TEST) {
		callback(api_film_city_list)
		return
	}
	$.get(WEBURL + "/api/film/city/list.action", {
		token: TOKEN
	}, function (res) {
		callback && callback(res)
	});
}

// 获得热映影片
function getHotList(cityId, callback) {
	if (TEST) {
		callback(api_film_hot_list)
		return
	}
	$.get(WEBURL + "/api/film/hot/list.action", {
		token: TOKEN,
		cityCode: cityId
	}, function (res) {
		callback && callback(res)
	});
}

// 获得即将上映
function getRightnowList(cityId, callback) {
	if (TEST) {
		callback(api_film_rightnow_list)
		return
	}
	$.get(WEBURL + "/api/film/rightnow/list.action", {
		token: TOKEN,
		City_code: cityId
	}, function (res) {
		callback && callback(res)
	});
}

// 查询影院列表
function getCinemaList(cityId,cityName, callback) {
	if (TEST) {
		callback(api_film_cinema_list)
		return
	}
	$.get(WEBURL + "/api/film/cinema/list.action", {
		token: TOKEN,
		cityCode: cityId,
		page_size:"1000",
		cityname:cityName
	}, function (res) {
		callback && callback(res)
	});
}

// 获得影院下的电影
function getFilmList(Cinema_id, callback) {
	if (TEST) {
		callback(api_film_list)
		return
	}
	$.get(WEBURL + "/api/film/list.action", {
		token: TOKEN,
		cinema_id:Cinema_id
	}, function (res) {
		callback && callback(res)
	});
}


// 获得电影 排期信息
function getFilmPaiqiList(Film_id, Cinema_id,callback) {
	console.log(Film_id, Cinema_id)
	if (TEST) {
		callback(api_film_paiqi_list)
		return
	}
	$.get(WEBURL + "/api/film/paiqi/list.action", {
		token: TOKEN,
		film_id:Film_id,
		cinema_id:Cinema_id
	}, function (res) {
		callback && callback(res)
	});
}

// 获取取座位图
function getSeatingList(id, callback) {
	if (TEST) {
		callback(api_film_seat_list2)
		return
	}
	$.get(WEBURL + "/api/film/seat/list.action", { 
		token: TOKEN,
		paiqi_id: id,
	}, function (res) {
		callback && callback(res)
	});
}

// api/film/seat/lock 锁座
function seatLook(data, callback) {
	if (TEST) {
		callback(api_film_seat_list3)
		return
	}
	$.get(WEBURL + "/api/film/seat/lock.action", {    
		token: TOKEN,
		seat_names: data.seat_names,
		paiqi_id: data.paiqi_id,
		seat_ids: data.seat_ids,
		phone_num: data.phone_num,
		zhengjianming: data.zhengjianming,
		zhengjiantype: data.zhengjiantype,
		zhengjianid: data.zhengjianid,
		seat_areas:data.seat_areas,
		account_id:data.account_id
	}, function (res) {
		callback && callback(res)
	});
}


// 刷新锁座
function refreshSeatLook(id, callback) {
	if (TEST) {
		callback(api_film_seat_list3)
		return
	}
	$.get(WEBURL + "/api/film/seat/refresh.action", {
		token: TOKEN,
		id
	}, function (res) {
		callback && callback(res)
	});
}


// 支付
function pay (id, callback) {
	if (TEST) {
		callback(api_film_seat_list3)
		return
	}
	$.get(WEBURL + "/api/film/seat/refresh.action", {
		token: TOKEN,
		id
	}, function (res) {
		callback && callback(res)
	});
}




















function timestampToTime123(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear();
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1);
	var D = date.getDate();
	var h = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();

	var mydate=new Date(Y+'-'+M+'-'+D);
	var myddy=mydate.getDay();//获取存储当前日期
	var weekday=["周日","周一","周二","周三","周四","周五","周六"];
	if (JSON.stringify(h).length == 1) {
		h = '0' + h
	}
	if (JSON.stringify(m).length == 1) {
		m = '0' + m
	}
	return Y+'-'+M+'-'+D+ ' '+ h + ':' +  m
}

function timestampToTime123123(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear();
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1);
	var D = date.getDate();
	var h = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();

	var mydate=new Date(Y+'-'+M+'-'+D);
	var myddy=mydate.getDay();//获取存储当前日期
	var weekday=["周日","周一","周二","周三","周四","周五","周六"];
	if (JSON.stringify(h).length == 1) {
		h = '0' + h
	}
	if (JSON.stringify(m).length == 1) {
		m = '0' + m
	}
	return Y+'-'+M+'-'+D+ ' '+ h + ':' +  m + ':' +  s
}

function timestampToTime(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear();
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1);
	var D = date.getDate();
	var h = date.getHours() + ':';
	var m = date.getMinutes() + ':';
	var s = date.getSeconds();

	var mydate=new Date(Y+'-'+M+'-'+D);
	var myddy=mydate.getDay();//获取存储当前日期
	var weekday=["周日","周一","周二","周三","周四","周五","周六"];
	
	return Y+'年'+M+'月'+D+'日' + "  " + weekday[myddy]
}



function timestampToTimeNoY(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear();
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1);
	var D = date.getDate();
	var h = date.getHours() + ':';
	var m = date.getMinutes() + ':';
	var s = date.getSeconds();

	var mydate=new Date(Y+'-'+M+'-'+D);
	var myddy=mydate.getDay();//获取存储当前日期
	var weekday=["周日","周一","周二","周三","周四","周五","周六"];
	
	return M+'月'+D+'日' + "  " + weekday[myddy]
}


function getSFTime(timestamp) {
	var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear();
	var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1);
	var D = date.getDate();
	var h = date.getHours() + ':';
	var m = date.getMinutes() ;
	var s = date.getSeconds();

	var mydate=new Date(Y+'-'+M+'-'+D);
	var myddy=mydate.getDay();//获取存储当前日期
	var weekday=["周日","周一","周二","周三","周四","周五","周六"];
	
	return h +  m
}





function getHashParameter(key) {
    var params = getHashParameters();
    return params[key];
}

function getHashParameters() {
    var arr = (location.hash || "").replace(/^\#/, '').split("&");
    var params = {};
    for (var i = 0; i < arr.length; i++) {
        var data = arr[i].split("=");
        if (data.length == 2) {
            params[data[0]] = data[1];
        }
    }
    return params;
}