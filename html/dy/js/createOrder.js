var cinemaData = JSON.parse(decodeURI(getHashParameter("data")))
var dyName = decodeURI(getHashParameter("name"))
var cinemaName = decodeURI(getHashParameter("cinemaname"))
var img = decodeURI(getHashParameter("img"))
var names = JSON.parse(decodeURI(getHashParameter("names")))
var orderData = JSON.parse(decodeURI(getHashParameter("orderData")))
var phone = decodeURI(getHashParameter("phone"))
var pqData = {}
var leftWidth = 0

$(".movie-details .title").html(dyName)
$(".movie-details .date").html(timestampToTime123(cinemaData.show_time) + ' ' + cinemaData.show_version)
$(".movie-details .local").html(cinemaName)
$(".movie-details .seats").html(`<div class="seats">${cinemaData.hall_name}  <div class="seat">${names.join(" ")}</div></div>`)
$(".movie-img img").attr('src', img)
$(".price-num").html('原价¥' + cinemaData.price / 100)
$(".price span").eq(0).html(orderData.Count + '张')
$(".mobile div").eq(1).html(phone)


// 点击支付
$(".price-btn").click(function() {
    refreshSeatLook(orderData.Order_Id, function(res) {
        // 还可以支付
        if (res.code == 200) {
            pay('11', function(res) {
                WeixinJSBridge.invoke("getBrandWCPayRequest", res, function (res) {
                    if (res.err_msg == "get_brand_wcpay_request:ok") {
                      Toast({
                        message: "支付成功",
                        duration: 2000,
                      });
                      that.$router.push({
                        name: "wodedingdan",
                        query: { activeVal: "00" },
                      });
                      //$location.path('/shopping/orderManagement')
                      // $scope.orderListQuery();
                      // 使用以上方式判断前端返回,微信团队郑重提示：
                      //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
                    }
                });
            }) 
        }
    })
})




setTime()
// 支付倒计时
function setTime() {
    let self = this
    setInterval(() => {
        var nowtime = new Date(), //获取当前时间
            endtime = new Date(timestampToTime123123(orderData.Expire_time).replace(/-/g, "/")); //定义结束时间
        var lefttime = endtime.getTime() - nowtime.getTime(); //距离结束时间的毫秒数
        var leftd = Math.floor(lefttime / (1000 * 60 * 60 * 24)); //计算天数
        var lefth = Math.floor(lefttime / (1000 * 60 * 60) % 24); //计算小时数
        var leftm = Math.floor(lefttime / (1000 * 60) % 60); //计算分钟数
        var lefts = Math.floor(lefttime / 1000 % 60); //计算秒数]
        if (JSON.stringify(lefth).length == 1) {
            lefth = '0' + lefth
        }
        if (JSON.stringify(leftm).length == 1) {
            leftm = '0' + leftm
        }
        if (JSON.stringify(lefts).length == 1) {
            lefts = '0' + lefts
        }
        self.time = {
            leftd,
            lefth,
            leftm,
            lefts
        }
        if (leftm > 0) {
            $(".cd-time").html(leftm + '' + lefts)
        }
        
    }, 1000)
}






var lock = false
$(".agreement-block .check-btn").click(function () {
    lock = true
    $(this).addClass('check-click')
    $(".continue-btn").addClass('active')
})
$(".continue-btn").click(function () {
    if (lock) {
        $(".agreement-section").hide()
    }
})
$("#back12").click(function () {
    window.history.back();
})

$("#priceDetail span").click(function () {
    $(".price-detail-modal").addClass('active')
    $("body").attr("data-dimmer", "modal")
    $(".body").addClass('noscroll')
    if ($("#priceDetail .arrow").hasClass('down')) {
        $("#priceDetail .arrow").removeClass('down').addClass('up')
    } else {
        $("#priceDetail .arrow").removeClass('up').addClass('down')
    }
})
$(".discount-amount").click(function () {
    $(".discount-modal").addClass('active')
    $("body").attr("data-dimmer", "modal")
    $(".body").addClass('noscroll')
})
$(".modal-title .close").click(function () {
    $("body").removeAttr("data-dimmer")
    $(".body").removeClass('noscroll')
    $(".modal").removeClass('active')
    $("#priceDetail .arrow").removeClass('down').addClass('up')
})